import javax.persistence.*;

@Entity
@Table(name="items")

public class Inventory {
    /** id is an identity type field in the database and the primary key */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;

    @Column(name = "Player")
    private String player;

    @Column(name = "Weapon")
    private String weapon;

    @Column(name = "Armor")
    private String armor;

    @Column(name = "Shoes")
    private String shoes;

    @Column(name = "Helmet")
    private String helmet;

    @Column(name = "Accessory")
    private String accessory;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }


    public String getArmor() {
        return armor;
    }

    public void setArmor(String armor) {
        this.armor = armor;
    }

    public String getShoes() {
        return shoes;
    }

    public void setShoes(String shoes) {
        this.shoes = shoes;
    }

    public String getHelmet() {
        return helmet;
    }

    public void setHelmet(String helmet) {
        this.helmet = helmet;
    }

    public String getAccessory() {
        return accessory;
    }

    public void setAccessory(String accessory) {
        this.accessory = accessory;
    }

    public String toString() {
        return "\n" + player + ":\n" + weapon + "\n" +
                armor + "\n" + shoes + "\n" + helmet + "\n" + accessory + "\n";
    }
}
