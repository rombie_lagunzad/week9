import org.hibernate.Session;
import java.util.*;

public class RunHibernate {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        boolean takeInput = true;

        TestNow t = TestNow.getInstance();

        do {
            try {
                // Ask the user to enter number which represent a certain character
                System.out.println("\nFind out the best build for your character!");
                System.out.println("Press 1 for Warrior");
                System.out.println("Press 2 for Sorcerer");
                System.out.println("Press 3 for Cleric");

                System.out.println("\nPlease enter the number: ");
                int num1 = input.nextInt();

                System.out.println("Accessing our database...");
                System.out.println("Please wait...");
                System.out.println(t.getInventory(num1));

                takeInput = false;

            } catch (InputMismatchException ex) {
                System.out.println("Invalid input! Please enter only number.");
                input.nextLine();
            }
        }
        while (takeInput);

        // Adding data in the item database
        Session session = HibernateUtility.getSessionFactory().openSession();
        session.beginTransaction();

        System.out.println("\nAdd the best build for your character!");
        Inventory items = new Inventory();

        System.out.println("\nPress enter the character class: ");
        String classChar = input.next();
        if (classChar.matches("[a-zA-Z ]+")) {
            items.setPlayer(classChar);
        } else {
            System.out.println("Invalid input! Please enter the character class: ");
            input.next();
        }

        System.out.println("\nPlease enter the weapon: ");
        String weapon = input.next();
        if (weapon.matches("[a-zA-Z ]+")) {
            items.setWeapon(weapon);
        }
        else {
            System.out.println("Invalid input! Please enter the weapon: ");
            input.next();
        }

        System.out.println("\nPlease enter the helmet: ");
        String helmet = input.next();
        if (helmet.matches("[a-zA-Z ]+")) {
            items.setHelmet(helmet);
        }
        else {
            System.out.println("Invalid input! Please enter the helmet: ");
            input.next();
        }

        System.out.println("\nPlease enter the armor: ");
        String armor = input.next();
        if (armor.matches("[a-zA-Z ]+")) {
            items.setArmor(armor);
        }
        else {
            System.out.println("Invalid input! Please enter the armor: ");
            input.next();
        }

        System.out.println("\nPlease enter the shoes: ");
        String shoes = input.next();
        if (shoes.matches("[a-zA-Z ]+")) {
            items.setShoes(shoes);
        }
        else {
            System.out.println("Invalid input! Please enter the shoes: ");
            input.next();
        }

        System.out.println("\nPlease enter the accessory: ");
        String accessory = input.next();
        if (accessory.matches("[a-zA-Z ]+")) {
            items.setAccessory(accessory);
        }
        else {
            System.out.println("Invalid input! Please enter the accessory: ");
            input.next();
        }

        //Save the items in the database
        session.save(items);

        //Commit the transaction
        session.getTransaction().commit();

        System.out.println("\nSuccessfully Added!");

        HibernateUtility.shutdown();
    }
}

