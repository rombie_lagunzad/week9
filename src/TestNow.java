import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.SessionFactory;

import javax.transaction.*;
import java.util.*;

public class TestNow {
    private static SessionBuilder<SessionBuilder> factory1;
    SessionFactory factory = null;
    Session session = null;

    private static TestNow single_instance = null;

    private TestNow() {
        factory = HibernateUtility.getSessionFactory();
    }

    public static TestNow getInstance() {
        if (single_instance == null) {
            single_instance = new TestNow();
        }

        return single_instance;
    }

    public List<Inventory> getInventory() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Inventory";
            List<Inventory> cs = (List<Inventory>) session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            System.out.println("Out of Range: Failed to open session!");
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    // Used to get the best items for a certain character by using id
    public Inventory getInventory(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Inventory where id = " + (id);
            Inventory c = (Inventory) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;

        } catch (Exception e) {
            System.out.println("Out of Range: Failed to open session!");
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}